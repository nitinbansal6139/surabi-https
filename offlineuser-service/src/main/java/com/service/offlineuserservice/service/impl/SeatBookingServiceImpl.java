package com.service.offlineuserservice.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.service.offlineuserservice.entity.Seat;
import com.service.offlineuserservice.repository.SeatDetailsRepository;
import com.service.offlineuserservice.service.SeatBookingService;

@Service
public class SeatBookingServiceImpl implements SeatBookingService{

	@Autowired
	SeatDetailsRepository seatDetails;
	
	@Override
	public String bookSeats(int totalSeats, Date bookingDate, String userName) {
		if ((bookingDate.getTime() - (new java.sql.Date(System.currentTimeMillis()).getTime()))==2 ){
		Seat addSeat = new Seat();
		addSeat.setTotalSeats(totalSeats);
		addSeat.setBookingDate(bookingDate);
		addSeat.setUserName(userName);
		seatDetails.saveAndFlush(addSeat);
		return "Seats booked";
		}
		else {
			return "You can book only 2 days in advance";
		}
		
	}

}
