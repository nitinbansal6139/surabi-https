package com.service.offlineuserservice.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.service.offlineuserservice.entity.FinancialReport;
import com.service.offlineuserservice.entity.Item;
import com.service.offlineuserservice.model.SelectItem;

@Service
public interface RestaurantItemService {

	List<Item> viewItemDetails();
	
	String selectItemDetailsByIds(List<SelectItem> selectItem);
	
	List<FinancialReport> viewFinalBill();
	
	String payBill(int amount, String paymentMethod);
	
	String giveReview(int rating, String comments);
}
