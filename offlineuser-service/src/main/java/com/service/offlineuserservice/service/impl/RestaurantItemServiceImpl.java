package com.service.offlineuserservice.service.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import com.service.offlineuserservice.entity.FinancialReport;
import com.service.offlineuserservice.entity.Item;
import com.service.offlineuserservice.model.SelectItem;
import com.service.offlineuserservice.repository.FinancialReportRepository;
import com.service.offlineuserservice.repository.ItemDetailsRepository;
import com.service.offlineuserservice.service.RestaurantItemService;

@Service
public class RestaurantItemServiceImpl implements RestaurantItemService {

	@Autowired
	FinancialReportRepository financialReportRepository;

	@Autowired
	ItemDetailsRepository itemDetailsRepository;

	@Autowired
	EntityManager entityManager;

	@Override
	public List<Item> viewItemDetails() {
		return itemDetailsRepository.findAll();
	}

	@Override
	public String selectItemDetailsByIds(List<SelectItem> selectItem) {
		TypedQuery<Integer> maxBillIdQuery = entityManager.createQuery("Select max(f.billId) from FinancialReport f",
				Integer.class);
		int billId;
		List<Integer> resultList = maxBillIdQuery.getResultList();
		if (resultList.isEmpty()) {
			billId = 1;
		} else {
			Integer maxBillId = resultList.get(0);
			if (maxBillId != null)
				billId = maxBillId + 1;
			else
				billId = 1;
		}
		Optional<Item> itemDetails;
		for (int i = 0; i < selectItem.size(); i++) {
			FinancialReport financialReport = new FinancialReport();
			itemDetails = itemDetailsRepository.findById(selectItem.get(i).getItemId());
			financialReport.setItemName(itemDetails.get().getName());

			financialReport.setUsername(selectItem.get(i).getUserName());
			financialReport.setBillId(billId);
			financialReport.setPurchaseDate(new java.sql.Date(System.currentTimeMillis()));
			financialReport.setQty(selectItem.get(i).getQty());
			financialReport.setPrice(itemDetails.get().getPrice() * selectItem.get(i).getQty());
			financialReportRepository.saveAndFlush(financialReport);
		}
		return "Items selected";
	}

	@Override
	public List<FinancialReport> viewFinalBill() {
		TypedQuery<Integer> maxBillIdQuery = entityManager.createQuery("Select max(f.billId) from FinancialReport f",
				Integer.class);
		int billId;
		List<Integer> resultList = maxBillIdQuery.getResultList();
		if (resultList.isEmpty()) {
			billId = 0;
		} else {
			Integer maxBillId = resultList.get(0);
			if (maxBillId != null)
				billId = maxBillId;
			else
				billId = 0;
		}
		if (billId != 0) {
			FinancialReport finalBill = new FinancialReport();
			finalBill.setBillId(billId);
			ExampleMatcher exampleMatcher = ExampleMatcher.matching()
					.withMatcher("finalcialReportToday", ExampleMatcher.GenericPropertyMatchers.exact())
					.withIgnorePaths("id", "username", "itemName", "qty", "price", "purchaseDate");
			Example<FinancialReport> example = Example.of(finalBill, exampleMatcher);
			return financialReportRepository.findAll(example);
		}
		return null;
	}

	@Override
	public String payBill(int amount, String paymentMethod) {
		return "Amount "+amount+" paid by "+paymentMethod ;
	}

	@Override
	public String giveReview(int rating, String comments) {
		return "Review added";
	}

}
