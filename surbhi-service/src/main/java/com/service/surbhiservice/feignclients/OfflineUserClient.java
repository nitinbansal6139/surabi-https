package com.service.surbhiservice.feignclients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.service.surbhiservice.model.FinancialReportDetails;
import com.service.surbhiservice.model.ItemDetails;
import com.service.surbhiservice.model.SelectItem;

@FeignClient(url = "https://localhost:8084/offlineUserServices", name = "OfflineUser-Client")
public interface OfflineUserClient {

	@GetMapping("/viewItems")
	public List<ItemDetails> getItems();

	@PostMapping("/selectItemsbyIds")
	public String selectItemByIds(@RequestParam("selectItem") List<SelectItem> selectItem);

	@GetMapping("/viewFinalBill")
	public List<FinancialReportDetails> getFinalBill();

	@PostMapping("/payBill")
	public String payBill(@RequestParam("amount") int amount, @RequestParam("paymentMethod") String paymentMethod);

	@PostMapping("/postReview")
	public String postReview(@RequestParam("rating") int rating, @RequestParam("comments") String comments);
}
