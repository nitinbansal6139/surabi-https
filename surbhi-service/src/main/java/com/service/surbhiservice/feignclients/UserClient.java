package com.service.surbhiservice.feignclients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.service.surbhiservice.model.FinancialReportDetails;
import com.service.surbhiservice.model.ItemDetails;
import com.service.surbhiservice.model.SelectItem;

@FeignClient(url = "https://localhost:8082/restaurant", name = "User-Client")
public interface UserClient {

	@GetMapping("/viewItems")
public List<ItemDetails> viewItems();
	
	@PostMapping("/selectItemsbyIds")
public String selectItemByIds(@RequestParam("selectItem") List<SelectItem> selectItem);
	
	@GetMapping("/viewFinalBill")
public List<FinancialReportDetails> getFinalBill();
}
