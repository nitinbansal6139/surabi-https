package com.service.surbhiservice.feignclients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(url = "https://localhost:8083/offlineUserServices", name = "FestiveOffer-Client")
public interface FestiveOfferClient {

	@PostMapping("/applyOffer")
public String applyOffer(@RequestParam("couponCode") String couponCode);
}
