package com.service.surbhiservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.service.surbhiservice.feignclients.OfflineUserClient;
import com.service.surbhiservice.model.FinancialReportDetails;
import com.service.surbhiservice.model.ItemDetails;
import com.service.surbhiservice.model.SelectItem;

@RestController("/offlineUsers")
public class OfflineUserController {

	@Autowired
	OfflineUserClient offlineUserClient;

	@GetMapping("/viewItemsFromSurabiOffline")
	public List<ItemDetails> getItemList() {
		return offlineUserClient.getItems();
	}

	@PostMapping("/selectItemsbyIdsFromSurabiOffline")
	public String selectItemByIds(@RequestBody List<SelectItem> selectItem) {
		return offlineUserClient.selectItemByIds(selectItem);
	}

	@GetMapping("/viewFinalBillFromSurabiOffline")
	public List<FinancialReportDetails> getFinalBill() {
		return offlineUserClient.getFinalBill();
	}

	@PostMapping("/payBillFromSurabiOffline")
	public String payBill(int amount, String paymentMethod) {
		return offlineUserClient.payBill(amount, paymentMethod);
	}

	@PostMapping("/postReviewFromSurabiOffline")
	public String postReview(int rating, String comments) {
		return offlineUserClient.postReview(rating, comments);
	}
}
