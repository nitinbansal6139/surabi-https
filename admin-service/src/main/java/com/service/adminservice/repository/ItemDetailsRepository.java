package com.service.adminservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.service.adminservice.entity.Item;

@Repository
public interface ItemDetailsRepository extends JpaRepository<Item, Integer>{
}
